/*
** utils.c for strace in /home/amstuta/rendu/AUSP_strace
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Apr 27 17:23:07 2015 arthur
** Last update Tue Apr 28 13:48:09 2015 arthur
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "strace.h"

void	ctrl_c(int sig)
{
  (void)sig;
  printf(" <unfinished ...>\nProcess %d detached\n", g_pid);
  exit(EXIT_SUCCESS);
}

void	usage()
{
  printf("Usage:\n\t./strace <command>\n");
  printf("\t./strace -p <pid>\n");
  exit(EXIT_FAILURE);
}
