##
## Makefile for makefile in /home/amstuta/rendu/AUSP_nmobjdump
##
## Made by arthur
## Login   <amstuta@epitech.net>
##
## Started on  Wed Apr  8 14:08:50 2015 arthur
## Last update Mon Apr 27 19:33:25 2015 arthur
##

RM	= rm -f

CC	= gcc
CFLAGS	= -Wall -Wextra -Werror

SRCS	= main.c \
	  aff.c \
	  args.c \
	  utils.c \
	  process.c \
	  new_systab.c

NAME	= strace

OBJS	= $(SRCS:.c=.o)

all:	   $(NAME)

$(NAME):   $(OBJS)
	   $(CC) $(OBJS) -o $(NAME)

clean:
	   $(RM) $(OBJS)

fclean:	   clean
	   $(RM) $(NAME)

re:	   fclean all

.PHONY:	   all clean fclean re
