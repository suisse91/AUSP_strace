/*
** aff.c for aff in /home/amstuta/rendu/AUSP_strace
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Apr 27 14:44:09 2015 arthur
** Last update Thu May 21 14:15:37 2015 arthur
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <asm/unistd_64.h>
#include "strace.h"

void		print_arg_str(pid_t pid, struct user_regs_struct *r,
				 unsigned long arg)
{
  if (r->orig_rax == __NR_read || r->orig_rax == __NR_write)
    r->rax != 0 ? print_args(pid, arg, r->rax) : printf("\"\"");
  else
    print_args(pid, arg, 0);
}

void		print_arg(pid_t pid, unsigned int pos,
			  struct user_regs_struct *r, unsigned long arg)
{
  if (strlen(g_syscall[(int)r->orig_rax].t_args) < pos)
    return ;
  if (pos > 0 && pos != (unsigned int)g_syscall[(int)r->orig_rax].n_args)
    printf(", ");
  if (g_syscall[(int)r->orig_rax].t_args[pos] == INT)
    printf("%ld", arg);
  else if (g_syscall[(int)r->orig_rax].t_args[pos] == STR)
    print_arg_str(pid, r, arg);
  else if (g_syscall[(int)r->orig_rax].t_args[pos] == ADDR)
    arg ? printf("0x%lx", arg) : printf("NULL");
  else if (g_syscall[(int)r->orig_rax].t_args[pos] == VOID)
    printf("void");
  else printf("%ld", arg);
}

void		init_registers(struct user_regs_struct *r,
			       unsigned long *registers)
{
  registers[0] = r->rdi;
  registers[1] = r->rsi;
  registers[2] = r->rdx;
  registers[3] = r->rcx;
  registers[4] = r->r8;
  registers[5] = r->r9;
  registers[6] = 0;
}

void   		print_all_args(pid_t pid, struct user_regs_struct *r)
{
  unsigned int 	i;
  unsigned long	registers[7];

  i = 1;
  init_registers(r, registers);
  if (g_syscall[(int)r->orig_rax].t_args != NULL)
    {
      printf("%s(", g_syscall[(int)r->orig_rax].name);
      while (i < strlen(g_syscall[(int)r->orig_rax].t_args))
	{
	  print_arg(pid, i - 1, r, registers[i - 1]);
	  ++i;
	}
      printf(")\t=\t");
      print_arg(pid, g_syscall[(int)r->orig_rax].n_args, r, r->rax);
      printf("\n");
    }
  else
    printf("Unimplemented system calls: %s().\n",
	   g_syscall[(int)r->orig_rax].name);
}

void		print_execve(int argc, char **argv)
{
  int		i;

  i = 1;
  printf("execve(\"%s\", [\"", argv[1]);
  while (i != argc)
    {
      printf("%s", argv[i]);
      i = i + 1;
    }
  printf("\"], [/* 46 vars */])\t=\t0\n");
}
