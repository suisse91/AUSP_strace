/*
** get_str.c for strace in /home/amstuta/rendu/AUSP_strace
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Apr 27 15:13:02 2015 arthur
** Last update Wed May 13 14:13:34 2015 arthur
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include "strace.h"

void		init_vars(int *max, int *size,
			  int *total, eBool *cont)
{
  *max = 0;
  if (*size > 25)
    {
      *size = 25;
      *max = 1;
    }
  *total = 0;
  *cont = true;
}

void		print_args(pid_t pid, unsigned long reg, int size)
{
  int		i;
  char		*str;
  int		total;
  eBool		cont;
  int		max;

  init_vars(&max, &size, &total, &cont);
  printf("\"");
  while (cont == true)
    {
      i = 0;
      str = (char *)ptrace(PTRACE_PEEKDATA, pid, (char *)(reg + total), 0);
      while (i < 8 &&
	     ((((char *)&str + i)[0] != '\0' && size == 0) || size != 0))
	{
	  isprint(((char *)&str + i)[0]) ? printf("%c", ((char *)&str + i)[0])
	    : printf("\\%o", ((char *)&str + i)[0]);
	  ++i;
	}
      total += i;
      if ((i < 8 && size == 0) || (total >= size && size > 0))
	cont = false;
    }
  max ? printf("\"...") : printf("\"");
}
