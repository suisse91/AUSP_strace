#include <stdlib.h>
#include "strace.h"

const t_syscall	g_syscall[] = 
{
	{ 3,		"read", "131"},
	{ 3,		"write", "131"},
	{ 2,		"open", "21"},
	{ 1,		"close", "1"},
	{ 2,		"stat", "23"},
	{ 2,		"fstat", "13"},
	{ 2,		"lstat", "23"},
	{ 3,		"poll", "301"},
	{ 3,		"lseek", "101"},
	{ 6,		"mmap", "311110"},
	{ 3,		"mprotect", "311"},
	{ 2,		"munmap", "31"},
	{ 1,		"brk", "3"},
	{ 3,		"sigaction", "133"},
	{ 3,		"sigprocmask", "133"},
	{ 1,		"sigreturn", "0"},
	{ 3,		"ioctl", "110"},
	{ 4,		"pread", "1310"},
	{ 4,		"pwrite", "1310"},
	{ 3,		"readv", "131"},
	{ 3,		"writev", "131"},
	{ 2,		"access", "21"},
	{ 1,		"pipe", "1"},
	{ 5,		"select", "13333"},
	{ 1,		"sched_yield", "0"},
	{ 5,		"mremap", "31113"},
	{ 3,		"msync", "311"},
	{ 3,		"mincore", "312"},
	{ 3,		"madvise", "311"},
	{ 3,		"shmget", "011"},
	{ 3,		"shmat", "131"},
	{ 3,		"shmctl", "113"},
	{ 1,		"dup", "1"},
	{ 2,		"dup2", "11"},
	{ 1,		"pause", "0"},
	{ 2,		"nanosleep", "33"},
	{ 2,		"getitimer", "13"},
	{ 1,		"alarm", "1"},
	{ 3,		"setitimer", "133"},
	{ 1,		"getpid", "0"},
	{ 4,		"sendfile", "1131"},
	{ 3,		"socket", "111"},
	{ 3,		"connect", "130"},
	{ 3,		"accept", "133"},
	{ 6,		"sendto", "131130"},
	{ 6,		"recvfrom", "131133"},
	{ 3,		"sendmsg", "131"},
	{ 3,		"recvmsg", "131"},
	{ 2,		"shutdown", "11"},
	{ 3,		"bind", "130"},
	{ 2,		"listen", "11"},
	{ 3,		"getsockname", "133"},
	{ 3,		"getpeername", "133"},
	{ 4,		"socketpair", "1111"},
	{ 5,		"setsockopt", "11130"},
	{ 5,		"getsockopt", "11133"},
	{ 5,		"clone", "13333"},
	{ 1,		"fork", "0"},
	{ 1,		"vfork", "0"},
	{ 3,		"execve", "222"},
	{ 1,		"exit", "1"},
	{ 4,		"wait4", "1313"},
	{ 2,		"kill", "11"},
	{ 1,		"uname", "3"},
	{ 3,		"semget", "011"},
	{ 3,		"semop", "131"},
	{ 4,		"semctl", "1110"},
	{ 1,		"shmdt", "3"},
	{ 2,		"msgget", "01"},
	{ 4,		"msgsnd", "1311"},
	{ 5,		"msgrcv", "13111"},
	{ 3,		"msgctl", "113"},
	{ 3,		"fcntl", "113"},
	{ 2,		"flock", "11"},
	{ 1,		"fsync", "1"},
	{ 1,		"fdatasync", "1"},
	{ 2,		"truncate", "20"},
	{ 2,		"ftruncate", "10"},
	{ 3,		"getdents", "131"},
	{ 0,		"getcwd", NULL},
	{ 1,		"chdir", "2"},
	{ 1,		"fchdir", "1"},
	{ 2,		"rename", "22"},
	{ 2,		"mkdir", "20"},
	{ 1,		"rmdir", "2"},
	{ 2,		"creat", "20"},
	{ 2,		"link", "22"},
	{ 1,		"unlink", "2"},
	{ 2,		"symlink", "22"},
	{ 3,		"readlink", "221"},
	{ 2,		"chmod", "20"},
	{ 2,		"fchmod", "10"},
	{ 3,		"chown", "200"},
	{ 3,		"fchown", "100"},
	{ 3,		"lchown", "200"},
	{ 1,		"umask", "0"},
	{ 2,		"gettimeofday", "33"},
	{ 2,		"getrlimit", "13"},
	{ 2,		"getrusage", "13"},
	{ 1,		"sysinfo", "3"},
	{ 1,		"times", "3"},
	{ 4,		"ptrace", "0133"},
	{ 1,		"getuid", "0"},
	{ 3,		"syslog", "121"},
	{ 1,		"getgid", "0"},
	{ 1,		"setuid", "0"},
	{ 1,		"setgid", "0"},
	{ 1,		"geteuid", "0"},
	{ 1,		"getegid", "0"},
	{ 2,		"setpgid", "11"},
	{ 1,		"getppid", "0"},
	{ 1,		"getpgrp", "1"},
	{ 1,		"setsid", "0"},
	{ 2,		"setreuid", "00"},
	{ 2,		"setregid", "00"},
	{ 2,		"getgroups", "10"},
	{ 2,		"setgroups", "13"},
	{ 3,		"setresuid", "000"},
	{ 3,		"getresuid", "333"},
	{ 3,		"setresgid", "000"},
	{ 3,		"getresgid", "333"},
	{ 1,		"getpgid", "1"},
	{ 1,		"setfsuid", "0"},
	{ 1,		"setfsgid", "0"},
	{ 1,		"getsid", "1"},
	{ 2,		"capget", "00"},
	{ 2,		"capset", "00"},
	{ 1,		"sigpending", "3"},
	{ 3,		"sigtimedwait", "333"},
	{ 3,		"sigqueueinfo", "113"},
	{ 1,		"sigsuspend", "3"},
	{ 2,		"sigaltstack", "33"},
	{ 2,		"utime", "23"},
	{ 3,		"mknod", "200"},
	{ 1,		"uselib", "2"},
	{ 1,		"personality", "1"},
	{ 2,		"ustat", "03"},
	{ 2,		"statfs", "23"},
	{ 2,		"fstatfs", "13"},
	{ 1,		"sysfs", "1"},
	{ 2,		"getpriority", "10"},
	{ 3,		"setpriority", "101"},
	{ 2,		"sched_setparam", "13"},
	{ 2,		"sched_getparam", "13"},
	{ 3,		"sched_setscheduler", "113"},
	{ 1,		"sched_getscheduler", "1"},
	{ 1,		"sched_get_priority_max", "1"},
	{ 1,		"sched_get_priority_min", "1"},
	{ 2,		"sched_rr_get_interval", "13"},
	{ 2,		"mlock", "31"},
	{ 2,		"munlock", "31"},
	{ 1,		"mlockall", "1"},
	{ 1,		"munlockall", "0"},
	{ 1,		"vhangup", "0"},
	{ 3,		"modify_ldt", "131"},
	{ 2,		"pivot_root", "22"},
	{ 1,		"_sysctl", "3"},
	{ 3,		"prctl", "111"},
	{ 2,		"arch_prctl", "11"},
	{ 1,		"adjtimex", "3"},
	{ 2,		"setrlimit", "13"},
	{ 1,		"chroot", "2"},
	{ 1,		"sync", "0"},
	{ 1,		"acct", "2"},
	{ 2,		"settimeofday", "33"},
	{ 5,		"mount", "22213"},
	{ 2,		"umount2", "21"},
	{ 2,		"swapon", "21"},
	{ 1,		"swapoff", "2"},
	{ 1,		"reboot", "1"},
	{ 2,		"sethostname", "21"},
	{ 2,		"setdomainname", "21"},
	{ 1,		"iopl", "1"},
	{ 3,		"ioperm", "111"},
	{ 2,		"create_module", "21"},
	{ 2,		"init_module", "23"},
	{ 1,		"delete_module", "2"},
	{ 1,		"get_kernel_syms", "3"},
	{ 5,		"query_module", "21313"},
	{ 4,		"quotactl", "1210"},
	{ 3,		"nfsservctl", "133"},
	{ 0,		"getpmsg", NULL},
	{ 0,		"putpmsg", NULL},
	{ 0,		"afs_syscall", NULL},
	{ 0,		"tuxcall", NULL},
	{ 0,		"security", NULL},
	{ 1,		"gettid", "0"},
	{ 3,		"readahead", "101"},
	{ 5,		"setxattr", "22311"},
	{ 5,		"lsetxattr", "22311"},
	{ 5,		"fsetxattr", "12311"},
	{ 4,		"getxattr", "2231"},
	{ 4,		"lgetxattr", "2231"},
	{ 4,		"fgetxattr", "1231"},
	{ 3,		"listxattr", "221"},
	{ 3,		"llistxattr", "221"},
	{ 3,		"flistxattr", "121"},
	{ 2,		"removexattr", "22"},
	{ 2,		"lremovexattr", "22"},
	{ 2,		"fremovexattr", "12"},
	{ 2,		"tkill", "11"},
	{ 1,		"time", "3"},
	{ 6,		"futex", "311331"},
	{ 3,		"sched_setaffinity", "113"},
	{ 3,		"sched_getaffinity", "113"},
	{ 1,		"set_thread_area", "3"},
	{ 2,		"io_setup", "03"},
	{ 1,		"io_destroy", "0"},
	{ 5,		"io_getevents", "01133"},
	{ 3,		"io_submit", "013"},
	{ 3,		"io_cancel", "033"},
	{ 1,		"get_thread_area", "3"},
	{ 3,		"lookup_dcookie", "021"},
	{ 1,		"epoll_create", "1"},
	{ 0,		"epoll_ctl_old", NULL},
	{ 0,		"epoll_wait_old", NULL},
	{ 5,		"remap_file_pages", "31111"},
	{ 3,		"getdents", "131"},
	{ 1,		"set_tid_address", "3"},
	{ 0,		"restasyscall", NULL},
	{ 4,		"semtimedop", "1313"},
	{ 0,		"fadvise", NULL},
	{ 3,		"timer_create", "033"},
	{ 4,		"timer_settime", "0133"},
	{ 2,		"timer_gettime", "03"},
	{ 1,		"timer_getoverrun", "0"},
	{ 1,		"timer_delete", "0"},
	{ 2,		"clock_settime", "03"},
	{ 2,		"clock_gettime", "03"},
	{ 2,		"clock_getres", "03"},
	{ 4,		"clock_nanosleep", "0133"},
	{ 1,		"exit_group", "1"},
	{ 4,		"epoll_wait", "1311"},
	{ 4,		"epoll_ctl", "1113"},
	{ 3,		"tgkill", "111"},
	{ 2,		"utimes", "20"},
	{ 0,		"vserver", NULL},
	{ 6,		"mbind", "311310"},
	{ 3,		"set_mempolicy", "131"},
	{ 5,		"get_mempolicy", "33111"},
	{ 0,		"mq_open", NULL},
	{ 0,		"mq_unlink", NULL},
	{ 0,		"mq_timedsend", NULL},
	{ 0,		"mq_timedreceive", NULL},
	{ 0,		"mq_notify", NULL},
	{ 3,		"mq_getsetattr", "033"},
	{ 4,		"kexec_load", "1131"},
	{ 4,		"waitid", "0031"},
	{ 5,		"add_key", "22310"},
	{ 4,		"request_key", "2220"},
	{ 2,		"keyctl", "10"},
	{ 3,		"ioprio_set", "111"},
	{ 2,		"ioprio_get", "11"},
	{ 1,		"inotify_init", "0"},
	{ 3,		"inotify_add_watch", "121"},
	{ 2,		"inotify_rm_watch", "11"},
	{ 4,		"migrate_pages", "1133"},
	{ 3,		"openat", "121"},
	{ 3,		"mkdirat", "120"},
	{ 4,		"mknodat", "1200"},
	{ 5,		"fchownat", "12001"},
	{ 3,		"futimesat", "120"},
	{ 0,		"newfstatat", NULL},
	{ 3,		"unlinkat", "121"},
	{ 4,		"renameat", "1212"},
	{ 5,		"linkat", "12121"},
	{ 3,		"symlinkat", "212"},
	{ 4,		"readlinkat", "1221"},
	{ 4,		"fchmodat", "1201"},
	{ 4,		"faccessat", "1211"},
	{ 0,		"pselect6", NULL},
	{ 4,		"ppoll", "3033"},
	{ 1,		"unshare", "1"},
	{ 2,		"set_robust_list", "31"},
	{ 3,		"get_robust_list", "133"},
	{ 6,		"splice", "131311"},
	{ 4,		"tee", "1111"},
	{ 4,		"sync_file_range", "1001"},
	{ 4,		"vmsplice", "1311"},
	{ 6,		"move_pages", "113331"},
	{ 4,		"utimensat", "1201"},
	{ 5,		"epoll_pwait", "13113"},
	{ 3,		"signalfd", "131"},
	{ 2,		"timerfd_create", "11"},
	{ 2,		"eventfd", "11"},
	{ 4,		"fallocate", "1100"},
	{ 4,		"timerfd_settime", "1133"},
	{ 2,		"timerfd_gettime", "13"},
	{ 4,		"accept4", "1331"},
	{ 0,		"signalfd4", NULL},
	{ 0,		"eventfd2", NULL},
	{ 1,		"epoll_create1", "1"},
	{ 3,		"dup3", "111"},
	{ 2,		"pipe2", "11"},
	{ 1,		"inotify_init1", "1"},
	{ 4,		"preadv", "1310"},
	{ 4,		"pwritev", "1310"},
	{ 4,		"tgsigqueueinfo", "1113"},
	{ 5,		"perf_event_open", "31111"},
	{ 5,		"recvmmsg", "13113"},
	{ 2,		"fanotify_init", "11"},
	{ 5,		"fanotify_mark", "11112"},
	{ 4,		"prlimit", "1133"},
	{ 5,		"name_to_handle_at", "12331"},
	{ 3,		"open_by_handle_at", "131"},
	{ 0,		"clock_adjtime", NULL},
	{ 1,		"syncfs", "1"},
	{ 4,		"sendmmsg", "1311"},
	{ 2,		"setns", "11"},
	{ 3,		"getcpu", "333"},
	{ 6,		"process_vm_readv", "131311"},
	{ 6,		"process_vm_writev", "131311"},
	{ 5,		"kcmp", "11111"},
	{ 3,		"finit_module", "121"},
	{ 3,		"sched_setattr", "131"},
	{ 4,		"sched_getattr", "1311"},
	{ 5,		"renameat2", "12121"},
	{ 3,		"seccomp", "113"},
	{ 0,		"getrandom", NULL},
	{ 2,		"memfd_create", "21"},
	{ 0,		"kexec_file_load", NULL},
	{ 0,		"bpf", NULL},
{ -1,		NULL, NULL}
};
