/*
** process.c for process in /home/amstuta/rendu/AUSP_strace
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Apr 27 17:29:28 2015 arthur
** Last update Wed May 13 16:34:27 2015 elkaim raphael
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "strace.h"

void				exec_steps(pid_t pid, eBool flag)
{
  struct user_regs_struct	r;
  int				status;

  wait(&status);
  if (flag == true)
    {
      v_ptrace(PT_STEP, pid, NULL, 0);
      wait(&status);
    }
  while (!WIFEXITED(status))
    {
      v_ptrace(PT_GETREGS, pid, NULL, &r);
      if ((int)r.orig_rax != -1 && (int)r.orig_rax < T_SIZE)
	print_all_args(pid, &r);
      v_ptrace(PT_STEP, pid, NULL, 0);
      wait(&status);
    }
}

void				fork_proc(char **args)
{
  pid_t				pid;

  if ((g_pid = pid = fork()) == -1)
    {
      printf("Couldn't launch executable\n");
      exit(EXIT_FAILURE);
    }
  if (pid == 0)
    {
      v_ptrace(PTRACE_TRACEME, 0, NULL, 0);
      if (execvp(args[0], args) == -1)
	exit(EXIT_FAILURE);
    }
  else
    exec_steps(pid, true);
}

void				pid_option(int ac, char **av)
{
  if (ac == 3 && !strcmp(av[1], "-p") && atoi(av[2]) != 0)
    {
      v_ptrace(PTRACE_ATTACH, atoi(av[2]), NULL, NULL);
      printf("Process %s attached\n", av[2]);
      exec_steps(atoi(av[2]), false);
      g_pid = atoi(av[2]);
    }
}
