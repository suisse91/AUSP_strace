/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Tue Apr 28 12:58:16 2015 arthur
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "strace.h"

int	g_pid = 0;

void	v_ptrace(enum __ptrace_request r, pid_t pid,
		 void *addr, void *data)
{
  if (ptrace(r, pid, addr, data) == -1)
    {
      printf("Ptrace error: ");
      fflush(stdout);
      perror("");
      exit(EXIT_FAILURE);
    }
}

int	main(int ac, char **av)
{
  if (ac < 2)
    usage();
  signal(SIGINT, ctrl_c);
  pid_option(ac, av);
  print_execve(ac, av);
  fork_proc(&av[1]);
  return (EXIT_SUCCESS);
}
