/*
** strace.h for strace in /home/amstuta/rendu/AUSP_strace
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Apr 27 11:05:19 2015 arthur
** Last update Wed May 13 14:13:21 2015 arthur
*/

#ifndef STRACE_H_
# define STRACE_H_

# include <sys/user.h>
# include <sys/types.h>
# include <sys/ptrace.h>

# define VOID	'0'
# define INT	'1'
# define STR	'2'
# define ADDR	'3'
# define CHAR	'4'
# define T_SIZE	332

typedef struct	s_syscall
{
  int		n_args;
  char		*name;
  char		*t_args;
}		t_syscall;

typedef enum
  {
    true,
    false
  }	eBool;

/*
** main.c
*/

void	v_ptrace(enum __ptrace_request, pid_t,
		 void*, void*);

/*
** aff.c
*/

void	print_arg_str(pid_t, struct user_regs_struct*, unsigned long);
void	print_arg(pid_t, unsigned int, struct user_regs_struct*,
		  unsigned long);
void	init_registers(struct user_regs_struct*, unsigned long*);
void	print_all_args(pid_t, struct user_regs_struct*);
void	print_execve(int, char**);

/*
** args.c
*/

void	init_vars(int*, int*, int*, eBool*);
void	print_args(pid_t, unsigned long, int);

/*
** process.c
*/

void	exec_steps(pid_t, eBool);
void	fork_proc(char**);
void	pid_option(int, char**);

/*
** utils.c
*/

void	usage();
void	ctrl_c(int);
char	*get_str(pid_t, unsigned long, unsigned long);

extern int		g_pid;
extern const t_syscall	g_syscall[];

#endif /* !STRACE_H_ */
